﻿namespace USManager
{
    partial class SelectType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnUltraStar = new Button();
            btnStepMania = new Button();
            label1 = new Label();
            SuspendLayout();
            // 
            // btnUltraStar
            // 
            btnUltraStar.Location = new Point(60, 102);
            btnUltraStar.Name = "btnUltraStar";
            btnUltraStar.Size = new Size(75, 23);
            btnUltraStar.TabIndex = 0;
            btnUltraStar.Text = "UltraStar";
            btnUltraStar.UseVisualStyleBackColor = true;
            btnUltraStar.Click += btnUltraStar_Click;
            // 
            // btnStepMania
            // 
            btnStepMania.Location = new Point(204, 102);
            btnStepMania.Name = "btnStepMania";
            btnStepMania.Size = new Size(83, 23);
            btnStepMania.TabIndex = 1;
            btnStepMania.Text = "StepMania";
            btnStepMania.UseVisualStyleBackColor = true;
            btnStepMania.Click += btnStepMania_Click;
            // 
            // label1
            // 
            label1.Location = new Point(12, 30);
            label1.Name = "label1";
            label1.Size = new Size(318, 38);
            label1.TabIndex = 2;
            label1.Text = "Bienvenido a Song Manager. ¿De que formato es la librería que quiere abrir?";
            label1.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // SelectType
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(342, 137);
            Controls.Add(label1);
            Controls.Add(btnStepMania);
            Controls.Add(btnUltraStar);
            Name = "SelectType";
            Text = "UltraStar/Stepmania Song Manager";
            ResumeLayout(false);
        }

        #endregion

        private Button btnUltraStar;
        private Button btnStepMania;
        private Label label1;
    }
}