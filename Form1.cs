using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Media;
using System.Numerics;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System.Text;
using System.Diagnostics;
using System.Data;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Button;
using Microsoft.VisualBasic;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TextBox;
using NAudio.Gui;
using System.Linq;

namespace USManager
{

    public partial class Form1 : Form
    {
        private BackgroundWorker bgWorker;

        private string[] files;
        private string songsFolder;
        private List<Dictionary<string, string>> dataSongs = new();
        //private DataTable DataSource = new();
        private WaveOutEvent? outputDevice;
        private AudioFileReader? audioFile;
        //Fila seleccionada en la lista.
        private int selectedRow = 0;
        private string TypeFolder;

        public Form1()
        {
            InitializeComponent();

            // Inicializa el BackgroundWorker
            bgWorker = new BackgroundWorker();
            bgWorker.DoWork += new DoWorkEventHandler(BgWorker_DoWork);
            bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BgWorker_RunWorkerCompleted);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using var sform = new SelectType();
            var result = sform.ShowDialog();
            if (result == DialogResult.OK) //Clicked a value
            {
                TypeFolder = sform.ReturnValue; //Return Value
            }
            else
            {
                throw new Exception("Error. No se seleccion� valor.");
            }

            //Directorio a recorrer
            if (songsFolder == null)
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    songsFolder = fbd.SelectedPath;
                }
                else
                {
                    MessageBox.Show("No se ha seleccionado una carpeta, se va a cerrar la aplicaci�n...");
                    Application.Exit();
                }
            }
        }


        private void Form1_Shown(object sender, EventArgs e)
        {

            //Inicia el subproceso pasando la ruta de la carpeta como argumento, despu�s de que el formulario est� cargado.
            this.Enabled = false; //Desabilitar todo mientras carga.
            bgWorker.RunWorkerAsync(songsFolder);


        }

        private void BgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Argument is null) //EXCEPTION!
            {
                return; //Cambiar por un error de aplicaci�n.
            }
            string folder = (string)e.Argument;
            ChangeStatusData("Obteniendo informaci�n de la carpeta " + folder + "...", 0);

            if (TypeFolder == "UltraStar")
            {
                // Obtiene una lista de archivos en la carpeta (incluye subcarpetas)
                files = Directory.GetFiles(folder, "*.txt", SearchOption.AllDirectories);
            }
            else if (TypeFolder == "StepMania")
            {
                // Obtiene una lista de archivos en la carpeta (incluye subcarpetas). Dwi y sm son distintas extensiones para ficheros de stepmania.
                files = Directory.GetFiles(folder, "*.dwi", SearchOption.AllDirectories)
                        .Union(Directory.GetFiles(folder, "*.sm", SearchOption.AllDirectories)).ToArray();
            }

            //Sacamos datos de las canciones
            int currentItem = 0;
            foreach (string file in files)
            {
                //Mostramos progreso..
                currentItem++;
                ChangeStatusData("Obteniendo informaci�n de " + file + "...", Convert.ToInt32(currentItem * 100 / files.Length));


                //Leemos informaci�n del fichero
                dataSongs.Add(ReadSongInfo(file));
            }

            //Terminado, limpiamos..
            ChangeStatusData("Preparando tabla de canciones...", 100);


        }

        private void ChangeStatusData(string text = "Listo", int percent = 100)
        {
            int valu = percent * toolStripProgressBar1.Maximum / 100; //Regla de 3 para poner el porcentaje.
            if (valu > toolStripProgressBar1.Maximum)
            {
                valu = toolStripProgressBar1.Maximum;
            }
            Invoke((MethodInvoker)delegate
            {
                toolStripProgressBar1.Value = Convert.ToInt32(valu);
                toolStripStatusLabel1.Text = text;
            });
        }
        private void ChangeStatusData(int percent = 100)
        {
            ChangeStatusData(toolStripStatusLabel1.Text, percent);
        }

        //Este worker genera la lista.. 
        private void BgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Establece los resultados para el evento RunWorkerCompleted

            dataGridView1.SuspendLayout();
            dataGridView1.Enabled = false;
            //Agregamos la columna checkbox
            DataGridViewCheckBoxColumn checkCol = new()
            {
                HeaderText = "SELECCIONADO",
                Name = "SELECCIONADO",
                FalseValue = false,
                TrueValue = true
            };
            dataGridView1.Columns.Add(checkCol);
            List<string> listacampos = new() { "PATH", "TITLE", "ARTIST", "GENRE", "LANGUAGE", "YEAR", "MP3", "MP3IC", "VIDEO", "VIDEOIC", "COVER", "COVERIC" };
            foreach (string key in listacampos)
            {
                if (key.EndsWith("IC"))
                {
                    DataGridViewImageColumn tic = new()
                    {
                        HeaderText = key[..^2], //Es lo mismo que Length-2
                        Name = key,
                        ImageLayout = DataGridViewImageCellLayout.Zoom
                    };
                    dataGridView1.Columns.Add(tic);
                }
                else
                {
                    dataGridView1.Columns.Add(key, key);
                }
                dataGridView1.Columns[key].ReadOnly = true;

            }
            //COLUMNAS OCULTAS
            List<string> tmplst = new() { "PATH", "MP3", "VIDEO", "COVER" };
            foreach (string key in tmplst)
            {
                dataGridView1.Columns[key].Visible = false;
            }

            //Agregamos las filas con los valores
            foreach (Dictionary<string, string> dic in dataSongs)
            {

                dataGridView1.Rows.Add(false);
                int i = dataGridView1.Rows.Count - 1;
                ChangeStatusData(i * 100 / files.Length);

                foreach (string key in listacampos)
                {
                    if (key.EndsWith("IC")) //Estos son los campos de imagen, tienen un igual pero sin el IC.
                    {
                        string k2 = key[..^2];
                        //Comprobar si de por si existe
                        if (dic.ContainsKey(k2))
                        {
                            string fpath = dic["FOLDERPATH"] + Path.DirectorySeparatorChar + dic[k2];
                            //Hay que ver si el archivo existe, porque si no existe, hay que poner el false!
                            if (File.Exists(fpath))
                            {
                                dataGridView1.Rows[i].Cells[key].Value = ImageHelper.GetImage("trueimg");
                            }
                            else
                            {
                                dataGridView1.Rows[i].Cells[key].Value = ImageHelper.GetImage("falseimg");
                            }
                        }
                        else // Si no existe la clave, es un false directamente.
                        {
                            dataGridView1.Rows[i].Cells[key].Value = ImageHelper.GetImage("falseimg");
                        }
                    }
                    else
                    {
                        try
                        {
                            dataGridView1.Rows[i].Cells[key].Value = dic[key];
                        }
                        catch
                        {
                            dataGridView1.Rows[i].Cells[key].Value = "";
                        }
                    }
                }
            }

            dataGridView1.ResumeLayout();
            dataGridView1.Enabled = true;

            //Filtros...
            ChangeStatusData("Preparando filtros...");
            //Preparamos los filtros de la izquierda.
            // Esto recoge los diferentes valores de "LANGUAGE"
            var langFilter = dataGridView1.Rows.OfType<DataGridViewRow>()
                                            .Select(r => r.Cells["LANGUAGE"].Value.ToString())
                                            .Distinct()
                                            .ToList();
            langFilter.Remove("");
            langFilter.Add("");
            filterLangCB.Enabled = false;
            filterLangCB.DataSource = langFilter;
            filterLangCB.SelectedIndex = filterLangCB.Items.Count - 1;
            filterLangCB.Enabled = true;
            // Esto recoge los diferentes valores de "GENRE"
            var genreFilter = dataGridView1.Rows.OfType<DataGridViewRow>()
                                            .Select(r => r.Cells["GENRE"].Value.ToString())
                                            .Distinct()
                                            .ToList();
            genreFilter.Remove("");
            genreFilter.Add("");
            filterGenreCB.Enabled = false;
            filterGenreCB.DataSource = genreFilter;
            filterGenreCB.SelectedIndex = filterGenreCB.Items.Count - 1;
            filterGenreCB.Enabled = true;
            this.Enabled = true;

            ChangeStatusData("Listo! " + (dataGridView1.Rows.Count - 1).ToString() + " canciones.");

        }

        private Dictionary<string, string> ReadSongInfo(string path)
        {
            Dictionary<string, string> fields = new();
            if (TypeFolder == "UltraStar")
            {
                fields = new()
                {
                    {"#GENRE", "GENRE" },
                    {"#LANGUAGE", "LANGUAGE" },
                    {"#YEAR", "YEAR" },
                    {"#MP3", "MP3" },
                    {"#COVER", "COVER" },
                    {"#TITLE", "TITLE" },
                    {"#ARTIST", "ARTIST" },
                    {"#VIDEO", "VIDEO" }
                };
            }
            else if (TypeFolder == "StepMania")
            {
                fields = new()
                {
                    {"#GENRE", "GENRE" },
                    {"#LANGUAGE", "LANGUAGE" },
                    {"#YEAR", "YEAR" },
                    {"#MUSIC", "MP3" },
                    {"#BANNER", "BANNER" },
                    {"#BACKGROUND","COVER" },
                    {"#TITLE", "TITLE" },
                    {"#ARTIST", "ARTIST" },
                    {"#VIDEO", "VIDEO" }
                };
            }
            Dictionary<string, string> data = new();
            data["PATH"] = path; //Para tenerlo.
            string namefile = Path.GetFileNameWithoutExtension(path);
            data["FOLDERPATH"] = Path.GetDirectoryName(path);
            Encoding enc = GetEncoding(path);
            using (StreamReader sr = new(path, enc))
            {
                string? line;
                while ((line = sr.ReadLine()) != null)
                {
                    int pos = line.IndexOf(':');
                    if (pos == -1)
                        continue;
                    string key = line[..pos];
                    if (fields.TryGetValue(key, out string? value))
                        data[value] = line[(pos + 1)..].TrimEnd(';').Trim(); ;
                }
            }
            if (path.Contains(".dwi")) //Stepmania
            {
                if (!data.ContainsKey("MP3"))
                {
                    if (File.Exists(namefile + ".ogg"))
                    {
                        data["MP3"] = namefile + ".ogg";
                    }
                    else
                    {
                        data["MP3"] = namefile + ".mp3";
                    }
                }
                if (!data.ContainsKey("COVER"))
                {
                    if (File.Exists(namefile + "-bg.png"))
                    {
                        data["COVER"] = namefile + "-bg.png";
                    }
                }
            }
            return data;
        }


        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count > 0)
            {
                selectedRow = dataGridView1.SelectedRows[0].Index;
                changedDataGridViewRow();
            }

        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // Verifica que la celda seleccionada es una fila y no un encabezado
            if (e.RowIndex != -1)
            {
                selectedRow = e.RowIndex;
                changedDataGridViewRow();
            }
        }

        //Cambiamos los datos de visualizaci�n.
        private void changedDataGridViewRow()
        {
            if (selectedRow <= 0)
            {
                return;
            }
            DataGridViewRow dataGrid = dataGridView1.Rows[selectedRow];
            string directory = Path.GetDirectoryName(dataGrid.Cells["PATH"].Value.ToString()) + Path.DirectorySeparatorChar;
            songPathLabel.Text = dataGrid.Cells["PATH"].Value.ToString();
            mp3PathLabel.Text = directory + dataGrid.Cells["MP3"].Value.ToString();
            songNameLabel.Text = dataGrid.Cells["TITLE"].Value.ToString();
            try
            {
                songCoverPicture.ImageLocation = directory + dataGrid.Cells["COVER"].Value.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al seleccionar la imagen " + ex.ToString(), "Error");
            }
            //Abrir letra.
            processLyrics(dataGrid.Cells["PATH"].Value.ToString());

        }

        private void processLyrics(string lyricspath)
        {
            richTextBox1.Clear();
            string[] lines = File.ReadAllLines(lyricspath, GetEncoding(lyricspath));
            foreach (string line in lines)
            {
                if (line.StartsWith(":") || line.StartsWith("*") || line.StartsWith("+"))
                {
                    string[] parts = line.Split(' ');
                    string result = "";
                    for (int i = 4; i < parts.Length; i++)
                    {
                        result += parts[i] + " ";
                    }
                    if (line.StartsWith("*")) //Negrita
                    {
                        richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold); //Esto solo aplica al siguiente Append.
                    }

                    richTextBox1.AppendText(result.TrimEnd());

                }
                else if (line.StartsWith("-"))
                {
                    richTextBox1.AppendText(Environment.NewLine);
                }
            }
            //Centrar texto.
            richTextBox1.SelectAll();
            richTextBox1.SelectionAlignment = HorizontalAlignment.Center;
        }

        private void OnButtonStopClick(object sender, EventArgs args)
        {
            outputDevice?.Stop();
        }

        private void OnPlaybackStopped(object sender, StoppedEventArgs args)
        {
            if (outputDevice != null)
            {
                outputDevice.Dispose();
                outputDevice = null;
            }
            if (audioFile != null)
            {
                audioFile.Dispose();
                audioFile = null;
            }
            btnPlayPause.Text = "Play";
            mp3PlayingLabel.Text = "";
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dataGridView1.HitTest(e.X, e.Y);
                if (hti.RowIndex != -1)
                {
                    // Selecciona la fila donde se ha hecho clic
                    dataGridView1.Rows[hti.RowIndex].Selected = true;
                    selectedRow = hti.RowIndex;
                    // Crea el men� de acciones
                    ContextMenuStrip menu = new ContextMenuStrip();
                    menu.Items.Add("Abrir la carpeta contenedora").Name = "OpenFolder";
                    menu.Items.Add("Abrir letra de la canci�n").Name = "OpenTXTFile";
                    menu.Items.Add("Enviar seleccionados a Playlist...").Name = "CreateSelPlaylist";
                    menu.Items.Add("Eliminar canci�n marcada...").Name = "DelSelectedSong";

                    // Asigna el men� al DataGridView
                    dataGridView1.ContextMenuStrip = menu;
                    // Asigna el evento para abrir la carpeta
                    menu.ItemClicked += new ToolStripItemClickedEventHandler(contextMenuStrip1_ItemClicked);
                    // Muestra el men�
                    menu.Show(dataGridView1, new Point(e.X, e.Y));
                }
            }
        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "OpenFolder":
                    if (selectedRow > 0)
                    {
                        string? paath = dataGridView1.Rows[selectedRow].Cells["PATH"].Value.ToString();
                        paath = Path.GetDirectoryName(paath) + Path.DirectorySeparatorChar;
                        if (paath != null)
                        {
                            Process.Start(new ProcessStartInfo()
                            {
                                FileName = paath,
                                UseShellExecute = true,
                                Verb = "open"
                            });
                        }
                    }
                    break;
                case "OpenTXTFile":
                    if (selectedRow > 0)
                    {
                        string? paath = dataGridView1.Rows[selectedRow].Cells["PATH"].Value.ToString();
                        if (paath != null)
                        {
                            Process.Start(new ProcessStartInfo()
                            {
                                FileName = paath,
                                UseShellExecute = true,
                                Verb = "open"
                            });
                        }
                    }
                    break;
                case "CreateSelPlaylist":
                    if (selectedRow > 0)
                    {
                        doCreateSelPlaylist();
                    }
                    break;

                case "DelSelectedSong":
                    if (selectedRow > 0)
                    {
                        string? title = dataGridView1.Rows[selectedRow].Cells["TITLE"].Value.ToString();
                        string? paath = dataGridView1.Rows[selectedRow].Cells["PATH"].Value.ToString();
                        paath = Path.GetDirectoryName(paath) + Path.DirectorySeparatorChar;
                        if (paath != null)
                        {
                            DialogResult dr = MessageBox.Show("Borrar la canci�n " + title,
                      "Borrar canci�n", MessageBoxButtons.YesNo);
                            switch (dr)
                            {
                                case DialogResult.Yes:
                                    Directory.Delete(paath, true);
                                    dataGridView1.Rows.RemoveAt(selectedRow);
                                    break;
                                case DialogResult.No:
                                    break;
                            }
                        }
                    }
                    break;
            }
        }

        /// Get Encoding from a file.
        public static Encoding GetEncoding(string filename)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                Ude.CharsetDetector cdet = new Ude.CharsetDetector();
                cdet.Feed(file);
                cdet.DataEnd();
                if (cdet.Charset != null)
                {
                    try
                    {
                        return Encoding.GetEncoding(cdet.Charset);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Problem detecting Encoding", ex.ToString());
                        return Encoding.Latin1;
                    }
                }
                else
                {
                    return Encoding.ASCII;
                }
            }
        }

        private void filterByCBInDataGridView()
        {
            if (bgWorker.IsBusy) // No funcionar mientras no est� terminado el bgWorker..
            {
                return;
            }
            string filterlang = filterLangCB.SelectedValue.ToString()?.ToLower() ?? "";
            string filtergenre = filterGenreCB.SelectedValue.ToString()?.ToLower() ?? "";
            string filtertitle = filterTitleTB.Text.Trim().ToLower();
            string filterartist = filterArtistTB.Text.Trim().ToLower();

            Invoke((MethodInvoker)delegate
            {
                toolStripStatusLabel1.Text = "Buscando...";
            });
            this.Enabled = false;
            dataGridView1.SuspendLayout();
            dataGridView1.Enabled = false;

            selectedall = false; //Para reiniciar el estado de seleccionado

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                row.Visible = true;
                row.Cells["SELECCIONADO"].Value = false;
                row.Visible = row.Cells["GENRE"].Value.ToString().ToLower().Contains(filtergenre)
                            && row.Cells["LANGUAGE"].Value.ToString().ToLower().Contains(filterlang)
                            && row.Cells["TITLE"].Value.ToString().ToLower().Contains(filtertitle)
                            && row.Cells["ARTIST"].Value.ToString().ToLower().Contains(filterartist);

            }

            this.Enabled = true;
            dataGridView1.ResumeLayout();
            dataGridView1.Enabled = true;

            ChangeStatusData("Listo!");

        }


        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            // Verifica que la celda seleccionada es una fila y no un encabezado
            if (e.RowIndex != -1)
            {
                selectedRow = e.RowIndex;
                changedDataGridViewRow();
            }
        }


        private List<int> getSelectedRows(bool forceone = false)
        {
            if (forceone)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                return new List<int> { index };
            }
            return dataGridView1.Rows
            .OfType<DataGridViewRow>()
            .Where(r => (bool)r.Cells["SELECCIONADO"].Value == true)
            .Select(r => r.Index)
            .ToList();
        }

        int song_index = 0;
        private void btnPlayPauseAll_Click(object sender, EventArgs e)
        {
            if (outputDevice != null)
            {
                OnButtonStopClick(sender, e);
                mp3PlayingLabel.Text = "En Pausa";
                return;
            }
            else
            {
                if (((System.Windows.Forms.Button)sender).Name == "btnPlayPause")
                {
                    song_index = -1;
                    playilst_play(true);
                    return;
                }
                else
                {
                    song_index = 0;
                }
                playilst_play();
            }

        }


        private void changePlayingLabelText()
        {
            string? tmpstr = dataGridView1.Rows[selectedRow].Cells["TITLE"].Value.ToString();
            if (tmpstr != null)
            {
                mp3PlayingLabel.Text = "Reproduciendo " + tmpstr + "...";
                btnPlayPause.Text = "Pausa";
            }
            else
            {
                MessageBox.Show("No hay canci�n seleccionada.");
            }
        }

        // this event occur when the playing file has ended
        private void playlistbackstopped(object sender, StoppedEventArgs e)
        {
            List<int> selectedRows = getSelectedRows();
            if (selectedRows.Count == 0)
            {
                OnPlaybackStopped(sender, e);
                return;
            }
            if (song_index < selectedRows.Count - 1)
            {
                song_index++;      // play the next file
                playilst_play();
            }
            else // Ya no hay m�s.. 
            {
                song_index = 0; //Reiniciamos la cola.
                OnPlaybackStopped(sender, e);
                return;
            }
            if (song_index < 0)
            {
                song_index = 0;
            }
            /*
            if (((System.Windows.Forms.Button)sender).Name != "btnPlayPause"){
                playilst_play();
            }
            */

        }


        private void playilst_play(bool forceone = false)
        {
            string to_play = "";
            List<int> selectedRows = getSelectedRows();
            if (selectedRows.Count() > 0 && !forceone)
            {
                DataGridViewRow currentrow = dataGridView1.Rows[selectedRows[song_index]];
                changedDataGridViewRow(selectedRows[song_index]);
                string directory = Path.GetDirectoryName(currentrow.Cells["PATH"].Value.ToString()) + Path.DirectorySeparatorChar;
                to_play = directory + currentrow.Cells["MP3"].Value.ToString();
            }
            else //Si no se seleccion� nada, cogemos la actual marcada, as� juntamos las playlist con el resto.. :)
            {
                to_play = mp3PathLabel.Text;
            }



            if (outputDevice == null)
            {
                outputDevice = new WaveOutEvent();
                outputDevice.PlaybackStopped += playlistbackstopped;
            }

            if (outputDevice.PlaybackState != PlaybackState.Paused)
            {
                if (Path.GetExtension(to_play).ToLower() == ".ogg")
                {
                    var vorbisStream = new NAudio.Vorbis.VorbisWaveReader(to_play);
                    outputDevice.Init(vorbisStream);
                }
                else
                {
                    audioFile = new AudioFileReader(to_play);
                    outputDevice.Init(audioFile);
                }

            }
            outputDevice.Play();
            changePlayingLabelText();
        }


        private void changedDataGridViewRow(int rownumber)
        {
            dataGridView1.CurrentCell = dataGridView1[0, rownumber];
            changedDataGridViewRow();
        }

        private void btnBackPlaylist_Click(object sender, EventArgs e)
        {
            song_index -= 2;
            outputDevice?.Stop();
        }

        private void btnNextPlaylist_Click(object sender, EventArgs e)
        {
            outputDevice?.Stop();
        }


        bool selectedall = false;
        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Visible == true) // No se puede cambiar donde no se ve :)
                {
                    row.Cells["SELECCIONADO"].Value = !selectedall;
                }
            }
            selectedall = !selectedall;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            filterByCBInDataGridView();
        }

        private void splitter1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            //TODO NOT WORKING!
            int newWidth = splitter1.Left - panel2.Left;
            panel2.Width = newWidth;
        }

        private void btnCreateSelPlaylist_Click(object sender, EventArgs e)
        {
            if (selectedRow > 0)
            {
                doCreateSelPlaylist();
            }
        }

        private void doCreateSelPlaylist()
        {
            List<int> selectedRows = getSelectedRows();

            if (selectedRows.Count < 1)
            {
                MessageBox.Show("No hay nada seleccionado.");
                return;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = "upl files (*.upl)|*.upl|All files (*.*)|*.*",
                FilterIndex = 1,
                RestoreDirectory = true
            };


            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filepath = saveFileDialog.FileName;
                string name = Path.GetFileNameWithoutExtension(filepath);



                List<string> fileWrite = new() { "######################################","#Ultrastar Deluxe Playlist Format v1.0",
                    "Playlist Nueva Lista de Reproducci�n with "+selectedRows.Count.ToString()+" Songs.", "######################################",
                    "#Name: "+name.ToUpperInvariant(), "#Songs:"};

                foreach (int x in selectedRows)
                {
                    DataGridViewRow currentrow = dataGridView1.Rows[x];
                    string? directoryPath = Path.GetFileName(Path.GetDirectoryName(currentrow.Cells["PATH"].Value.ToString()));
                    if (directoryPath != null)
                    {
                        fileWrite.Add(directoryPath.Replace(" - ", " : "));
                    }

                }
                File.WriteAllLines(filepath, fileWrite);
                MessageBox.Show("Guardado en " + filepath);
            }
        }

        private void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                dataGridView1.ClearSelection();
                dataGridView1.Rows[e.RowIndex].Selected = true;

            }
        }

        private void btnPlayPause_Click(object sender, EventArgs e)
        {
            btnPlayPauseAll_Click(sender, e);
        }

        private void btnDeleteSongs_Click(object sender, EventArgs e)
        {
            List<int> selectedRows = getSelectedRows();
            selectedRows.Sort((a,b) => b.CompareTo(a)); //Ordenar descendentemente, del m�s alto al m�s bajo.

            if (selectedRows.Count < 1)
            {
                MessageBox.Show("No hay nada seleccionado.");
                return;
            }
            DialogResult dr = MessageBox.Show("Borrar "+ selectedRows.Count.ToString() +" canciones? Es irreversible",
      "Borrar canciones seleccionadas", MessageBoxButtons.YesNo);
            switch (dr)
            {
                case DialogResult.Yes:
                    foreach (var line in selectedRows)
                    {
                        string? paath = dataGridView1.Rows[line].Cells["PATH"].Value.ToString();
                        paath = Path.GetDirectoryName(paath) + Path.DirectorySeparatorChar;
                        if (paath != null)
                        {
                            Directory.Delete(paath, true);
                            dataGridView1.Rows.RemoveAt(line);
                        }
                    }

                    break;
                case DialogResult.No:
                    break;
            }
        }
    }

}