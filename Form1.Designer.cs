﻿namespace USManager
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            menuStrip1 = new MenuStrip();
            archivoToolStripMenuItem = new ToolStripMenuItem();
            acercaDeToolStripMenuItem = new ToolStripMenuItem();
            statusStrip1 = new StatusStrip();
            toolStripProgressBar1 = new ToolStripProgressBar();
            toolStripStatusLabel1 = new ToolStripStatusLabel();
            panel1 = new Panel();
            groupBox2 = new GroupBox();
            btnDeleteSongs = new Button();
            btnCreateSelPlaylist = new Button();
            btnSelectAll = new Button();
            groupBox1 = new GroupBox();
            btnSearch = new Button();
            label7 = new Label();
            filterArtistTB = new TextBox();
            label6 = new Label();
            filterGenreCB = new ComboBox();
            label5 = new Label();
            filterTitleTB = new TextBox();
            label4 = new Label();
            filterLangCB = new ComboBox();
            panel2 = new Panel();
            splitter1 = new Splitter();
            richTextBox1 = new RichTextBox();
            label8 = new Label();
            songCoverPicture = new PictureBox();
            songNameLabel = new Label();
            label1 = new Label();
            songPathLabel = new Label();
            label3 = new Label();
            panel4 = new Panel();
            btnNextPlaylist = new Button();
            btnBackPlaylist = new Button();
            btnPlayPauseAll = new Button();
            mp3PlayingLabel = new Label();
            label2 = new Label();
            mp3PathLabel = new Label();
            btnPlayPause = new Button();
            panel3 = new Panel();
            dataGridView1 = new DataGridView();
            groupBox3 = new GroupBox();
            menuStrip1.SuspendLayout();
            statusStrip1.SuspendLayout();
            panel1.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox1.SuspendLayout();
            panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)songCoverPicture).BeginInit();
            panel4.SuspendLayout();
            panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            groupBox3.SuspendLayout();
            SuspendLayout();
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { archivoToolStripMenuItem, acercaDeToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(1225, 24);
            menuStrip1.TabIndex = 3;
            menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            archivoToolStripMenuItem.Size = new Size(60, 20);
            archivoToolStripMenuItem.Text = "Archivo";
            // 
            // acercaDeToolStripMenuItem
            // 
            acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            acercaDeToolStripMenuItem.Size = new Size(71, 20);
            acercaDeToolStripMenuItem.Text = "Acerca de";
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { toolStripProgressBar1, toolStripStatusLabel1 });
            statusStrip1.Location = new Point(0, 543);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(1225, 22);
            statusStrip1.TabIndex = 4;
            statusStrip1.Text = "statusStrip1";
            // 
            // toolStripProgressBar1
            // 
            toolStripProgressBar1.Maximum = 1000;
            toolStripProgressBar1.Name = "toolStripProgressBar1";
            toolStripProgressBar1.Size = new Size(100, 16);
            // 
            // toolStripStatusLabel1
            // 
            toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            toolStripStatusLabel1.Size = new Size(118, 17);
            toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // panel1
            // 
            panel1.Controls.Add(groupBox2);
            panel1.Controls.Add(groupBox1);
            panel1.Dock = DockStyle.Left;
            panel1.Location = new Point(0, 24);
            panel1.Name = "panel1";
            panel1.Size = new Size(220, 519);
            panel1.TabIndex = 5;
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(btnDeleteSongs);
            groupBox2.Controls.Add(btnCreateSelPlaylist);
            groupBox2.Controls.Add(btnSelectAll);
            groupBox2.Location = new Point(3, 182);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(211, 114);
            groupBox2.TabIndex = 1;
            groupBox2.TabStop = false;
            groupBox2.Text = "Operaciones";
            // 
            // btnDeleteSongs
            // 
            btnDeleteSongs.Location = new Point(9, 80);
            btnDeleteSongs.Name = "btnDeleteSongs";
            btnDeleteSongs.Size = new Size(196, 23);
            btnDeleteSongs.TabIndex = 3;
            btnDeleteSongs.Text = "Eliminar canciones seleccionadas";
            btnDeleteSongs.UseVisualStyleBackColor = true;
            btnDeleteSongs.Click += btnDeleteSongs_Click;
            // 
            // btnCreateSelPlaylist
            // 
            btnCreateSelPlaylist.Location = new Point(9, 51);
            btnCreateSelPlaylist.Name = "btnCreateSelPlaylist";
            btnCreateSelPlaylist.Size = new Size(196, 23);
            btnCreateSelPlaylist.TabIndex = 2;
            btnCreateSelPlaylist.Text = "Crear Playlist con seleccionados";
            btnCreateSelPlaylist.UseVisualStyleBackColor = true;
            btnCreateSelPlaylist.Click += btnCreateSelPlaylist_Click;
            // 
            // btnSelectAll
            // 
            btnSelectAll.Location = new Point(9, 22);
            btnSelectAll.Name = "btnSelectAll";
            btnSelectAll.Size = new Size(196, 23);
            btnSelectAll.TabIndex = 0;
            btnSelectAll.Text = "Seleccionar Todo";
            btnSelectAll.UseVisualStyleBackColor = true;
            btnSelectAll.Click += btnSelectAll_Click;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(btnSearch);
            groupBox1.Controls.Add(label7);
            groupBox1.Controls.Add(filterArtistTB);
            groupBox1.Controls.Add(label6);
            groupBox1.Controls.Add(filterGenreCB);
            groupBox1.Controls.Add(label5);
            groupBox1.Controls.Add(filterTitleTB);
            groupBox1.Controls.Add(label4);
            groupBox1.Controls.Add(filterLangCB);
            groupBox1.Location = new Point(3, 3);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(211, 173);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "Búsqueda";
            // 
            // btnSearch
            // 
            btnSearch.Location = new Point(9, 144);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(196, 23);
            btnSearch.TabIndex = 8;
            btnSearch.Text = "Buscar";
            btnSearch.UseVisualStyleBackColor = true;
            btnSearch.Click += btnSearch_Click;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(6, 112);
            label7.Name = "label7";
            label7.Size = new Size(44, 15);
            label7.TabIndex = 7;
            label7.Text = "Artista:";
            // 
            // filterArtistTB
            // 
            filterArtistTB.Location = new Point(59, 109);
            filterArtistTB.Name = "filterArtistTB";
            filterArtistTB.Size = new Size(146, 23);
            filterArtistTB.TabIndex = 6;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(6, 54);
            label6.Name = "label6";
            label6.Size = new Size(48, 15);
            label6.TabIndex = 5;
            label6.Text = "Género:";
            // 
            // filterGenreCB
            // 
            filterGenreCB.FormattingEnabled = true;
            filterGenreCB.Location = new Point(59, 51);
            filterGenreCB.Name = "filterGenreCB";
            filterGenreCB.Size = new Size(146, 23);
            filterGenreCB.TabIndex = 4;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(6, 83);
            label5.Name = "label5";
            label5.Size = new Size(40, 15);
            label5.TabIndex = 3;
            label5.Text = "Título:";
            // 
            // filterTitleTB
            // 
            filterTitleTB.Location = new Point(59, 80);
            filterTitleTB.Name = "filterTitleTB";
            filterTitleTB.Size = new Size(146, 23);
            filterTitleTB.TabIndex = 2;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(6, 25);
            label4.Name = "label4";
            label4.Size = new Size(47, 15);
            label4.TabIndex = 1;
            label4.Text = "Idioma:";
            // 
            // filterLangCB
            // 
            filterLangCB.FormattingEnabled = true;
            filterLangCB.Location = new Point(59, 22);
            filterLangCB.Name = "filterLangCB";
            filterLangCB.Size = new Size(146, 23);
            filterLangCB.TabIndex = 0;
            // 
            // panel2
            // 
            panel2.Controls.Add(splitter1);
            panel2.Controls.Add(richTextBox1);
            panel2.Controls.Add(label8);
            panel2.Controls.Add(songCoverPicture);
            panel2.Controls.Add(songNameLabel);
            panel2.Controls.Add(label1);
            panel2.Dock = DockStyle.Right;
            panel2.Location = new Point(969, 24);
            panel2.Name = "panel2";
            panel2.Size = new Size(256, 519);
            panel2.TabIndex = 6;
            // 
            // splitter1
            // 
            splitter1.Location = new Point(0, 0);
            splitter1.Name = "splitter1";
            splitter1.Size = new Size(3, 519);
            splitter1.TabIndex = 5;
            splitter1.TabStop = false;
            splitter1.SplitterMoved += splitter1_SplitterMoved;
            // 
            // richTextBox1
            // 
            richTextBox1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            richTextBox1.Location = new Point(0, 285);
            richTextBox1.Name = "richTextBox1";
            richTextBox1.Size = new Size(256, 234);
            richTextBox1.TabIndex = 4;
            richTextBox1.Text = "";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(6, 267);
            label8.Name = "label8";
            label8.Size = new Size(36, 15);
            label8.TabIndex = 3;
            label8.Text = "Letra:";
            // 
            // songCoverPicture
            // 
            songCoverPicture.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            songCoverPicture.Location = new Point(6, 3);
            songCoverPicture.Name = "songCoverPicture";
            songCoverPicture.Size = new Size(247, 154);
            songCoverPicture.SizeMode = PictureBoxSizeMode.Zoom;
            songCoverPicture.TabIndex = 2;
            songCoverPicture.TabStop = false;
            // 
            // songNameLabel
            // 
            songNameLabel.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            songNameLabel.Location = new Point(9, 182);
            songNameLabel.Name = "songNameLabel";
            songNameLabel.Size = new Size(244, 19);
            songNameLabel.TabIndex = 1;
            songNameLabel.Text = "...";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(9, 161);
            label1.Name = "label1";
            label1.Size = new Size(127, 15);
            label1.TabIndex = 0;
            label1.Text = "Nombre de la canción:";
            // 
            // songPathLabel
            // 
            songPathLabel.Location = new Point(58, 18);
            songPathLabel.Name = "songPathLabel";
            songPathLabel.Size = new Size(634, 16);
            songPathLabel.TabIndex = 3;
            songPathLabel.Text = "...";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(15, 19);
            label3.Name = "label3";
            label3.Size = new Size(34, 15);
            label3.TabIndex = 2;
            label3.Text = "Ruta:";
            // 
            // panel4
            // 
            panel4.Controls.Add(groupBox3);
            panel4.Controls.Add(btnNextPlaylist);
            panel4.Controls.Add(btnBackPlaylist);
            panel4.Controls.Add(btnPlayPauseAll);
            panel4.Controls.Add(mp3PlayingLabel);
            panel4.Controls.Add(btnPlayPause);
            panel4.Dock = DockStyle.Bottom;
            panel4.Location = new Point(220, 407);
            panel4.Name = "panel4";
            panel4.Size = new Size(749, 136);
            panel4.TabIndex = 1;
            // 
            // btnNextPlaylist
            // 
            btnNextPlaylist.Location = new Point(292, 102);
            btnNextPlaylist.Name = "btnNextPlaylist";
            btnNextPlaylist.Size = new Size(124, 23);
            btnNextPlaylist.TabIndex = 9;
            btnNextPlaylist.Text = "Siguiente";
            btnNextPlaylist.UseVisualStyleBackColor = true;
            btnNextPlaylist.Click += btnNextPlaylist_Click;
            // 
            // btnBackPlaylist
            // 
            btnBackPlaylist.Location = new Point(162, 102);
            btnBackPlaylist.Name = "btnBackPlaylist";
            btnBackPlaylist.Size = new Size(124, 23);
            btnBackPlaylist.TabIndex = 8;
            btnBackPlaylist.Text = "Anterior";
            btnBackPlaylist.UseVisualStyleBackColor = true;
            btnBackPlaylist.Click += btnBackPlaylist_Click;
            // 
            // btnPlayPauseAll
            // 
            btnPlayPauseAll.Location = new Point(6, 102);
            btnPlayPauseAll.Name = "btnPlayPauseAll";
            btnPlayPauseAll.Size = new Size(150, 23);
            btnPlayPauseAll.TabIndex = 7;
            btnPlayPauseAll.Text = "Play Selected";
            btnPlayPauseAll.UseVisualStyleBackColor = true;
            btnPlayPauseAll.Click += btnPlayPauseAll_Click;
            // 
            // mp3PlayingLabel
            // 
            mp3PlayingLabel.Location = new Point(102, 77);
            mp3PlayingLabel.Name = "mp3PlayingLabel";
            mp3PlayingLabel.Size = new Size(581, 18);
            mp3PlayingLabel.TabIndex = 6;
            mp3PlayingLabel.Text = "...";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(15, 39);
            label2.Name = "label2";
            label2.Size = new Size(61, 15);
            label2.TabIndex = 4;
            label2.Text = "Ruta MP3:";
            // 
            // mp3PathLabel
            // 
            mp3PathLabel.Location = new Point(82, 38);
            mp3PathLabel.Name = "mp3PathLabel";
            mp3PathLabel.Size = new Size(610, 16);
            mp3PathLabel.TabIndex = 5;
            mp3PathLabel.Text = "...";
            // 
            // btnPlayPause
            // 
            btnPlayPause.Location = new Point(6, 73);
            btnPlayPause.Name = "btnPlayPause";
            btnPlayPause.Size = new Size(75, 23);
            btnPlayPause.TabIndex = 0;
            btnPlayPause.Text = "Play";
            btnPlayPause.UseVisualStyleBackColor = true;
            btnPlayPause.Click += btnPlayPause_Click;
            // 
            // panel3
            // 
            panel3.Controls.Add(dataGridView1);
            panel3.Dock = DockStyle.Fill;
            panel3.Location = new Point(220, 24);
            panel3.Name = "panel3";
            panel3.Size = new Size(749, 383);
            panel3.TabIndex = 7;
            // 
            // dataGridView1
            // 
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.AllowUserToOrderColumns = true;
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = SystemColors.Control;
            dataGridViewCellStyle1.Font = new Font("Segoe UI Semibold", 9F, FontStyle.Bold, GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = Color.White;
            dataGridViewCellStyle1.SelectionForeColor = Color.Black;
            dataGridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView1.Dock = DockStyle.Fill;
            dataGridView1.Location = new Point(0, 0);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.RowHeadersVisible = false;
            dataGridView1.RowHeadersWidth = 38;
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Font = new Font("Microsoft Sans Serif", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle2;
            dataGridView1.RowTemplate.Height = 25;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.Size = new Size(749, 383);
            dataGridView1.TabIndex = 1;
            dataGridView1.CellClick += dataGridView1_CellClick;
            dataGridView1.CellEnter += dataGridView1_CellEnter;
            dataGridView1.CellMouseDown += dataGridView1_CellMouseDown;
            dataGridView1.SelectionChanged += dataGridView1_SelectionChanged;
            dataGridView1.MouseClick += dataGridView1_MouseClick;
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(label3);
            groupBox3.Controls.Add(songPathLabel);
            groupBox3.Controls.Add(mp3PathLabel);
            groupBox3.Controls.Add(label2);
            groupBox3.Location = new Point(3, 6);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(743, 61);
            groupBox3.TabIndex = 10;
            groupBox3.TabStop = false;
            groupBox3.Text = "Archivo seleccionado";
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1225, 565);
            Controls.Add(panel3);
            Controls.Add(panel4);
            Controls.Add(panel2);
            Controls.Add(panel1);
            Controls.Add(statusStrip1);
            Controls.Add(menuStrip1);
            Name = "Form1";
            Text = "Song Manager";
            Load += Form1_Load;
            Shown += Form1_Shown;
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            panel1.ResumeLayout(false);
            groupBox2.ResumeLayout(false);
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)songCoverPicture).EndInit();
            panel4.ResumeLayout(false);
            panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem archivoToolStripMenuItem;
        private ToolStripMenuItem acercaDeToolStripMenuItem;
        private StatusStrip statusStrip1;
        private Panel panel1;
        private Panel panel2;
        private Panel panel4;
        private Button btnPlayPause;
        private Label songNameLabel;
        private Label label1;
        private Panel panel3;
        private Label songPathLabel;
        private Label label3;
        private DataGridView dataGridView1;
        private ToolStripStatusLabel toolStripStatusLabel1;
        public ToolStripProgressBar toolStripProgressBar1;
        private Label label2;
        private Label mp3PathLabel;
        private Label mp3PlayingLabel;
        private GroupBox groupBox1;
        private Label label4;
        private ComboBox filterLangCB;
        private PictureBox songCoverPicture;
        private Label label6;
        private ComboBox filterGenreCB;
        private Label label5;
        private TextBox filterTitleTB;
        private Button btnPlayPauseAll;
        private Button btnNextPlaylist;
        private Button btnBackPlaylist;
        private Label label7;
        private TextBox filterArtistTB;
        private GroupBox groupBox2;
        private Button btnSelectAll;
        private Button btnSearch;
        private RichTextBox richTextBox1;
        private Label label8;
        private Splitter splitter1;
        private Button btnCreateSelPlaylist;
        private Button btnDeleteSongs;
        private GroupBox groupBox3;
    }
}