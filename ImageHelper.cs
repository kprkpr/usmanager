﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USManager
{

    class ImageHelper
    {
        private static Dictionary<string, Image> imageCache = new ();

        public static Image? GetImage(string imageName)
        {
            // Comprueba si la imagen ya está en el diccionario
            if (imageCache.ContainsKey(imageName))
            {
                return imageCache[imageName];
            }
            else
            {
                // Carga la imagen desde los recursos

                // Añade la imagen al diccionario
                if (Resource1.ResourceManager.GetObject(imageName) is Image image)
                {
                    imageCache.Add(imageName, image);

                    return image;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
