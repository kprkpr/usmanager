﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace USManager
{
    public partial class SelectType : Form
    {
        public string ReturnValue { get; set; }

        public SelectType()
        {
            InitializeComponent();
        }

        private void btnUltraStar_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "UltraStar";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnStepMania_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "StepMania";
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
